#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#ifndef IMAGE
#define IMAGE
struct pixel { uint8_t b, g, r; };

#pragma pack(push, 1) 
struct image {
  uint64_t width, height;
  struct pixel* data;
};
#pragma pack(pop)
#endif

struct image image_create(size_t   width, size_t   height);

void image_destroy(struct image* img);
