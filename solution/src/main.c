#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include "rwfun.h"
#include <stdio.h>

static void std_output(const char* s);
static void std_error_output(const char* s);
const char* write_s_output[] = {
    [WRITE_OK] = "completed\n",
    [WRITE_ERROR] = "error\n",
    [FILE_WRITE_ERROR] = "error\n",
    [SUCCESSFUL_FILE_WRITE] = "completed\n"
};

const char* read_s_output[] = {
    [READ_OK] = "successfully read\n",
    [READ_INVALID_HEADER] = " reading error\n",
    [SUCCESSFUL_FILE_READ] = " successfully read\n",
    [FILE_READ_ERROR] = "reading error\n"
};
int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3 || !argv || !argv[1] || !argv[2]) {
        fprintf(stderr, "%s", "Incorrect command format, please input images");
        return 1;
    }
    
        struct image source = { 0 };
        struct image result = { 0 };
        enum read_status read_image_s = get_image(argv[1], &source);
        if (read_image_s >= 2) {
            std_error_output(read_s_output[read_image_s]);
            return 0;
        }
        std_output(read_s_output[read_image_s]);
        result = rotate(source);
        enum write_status write_image_s = set_image(argv[2], &result);
        if (write_image_s >= 2) {
            std_error_output(write_s_output[write_image_s]);
            return 0;
        }
        std_output(write_s_output[write_image_s]);
        image_destroy(&result);
        image_destroy(&source);
    
    return 0;
}

static void std_output(const char* s) {
    fprintf(stdout, "%s", s);
}

static void std_error_output(const char* s) {
    fprintf(stderr, "%s", s);
}
